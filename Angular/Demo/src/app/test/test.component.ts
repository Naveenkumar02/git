import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {
     
   id : number;
   name : string;
   avg :number;
   address:any;
   hobbies:any;


  constructor(){
   
    //alert("Constructor invoked....");
       
     this.id=101;
     this.name='naveen';
     this.avg= 45.6;

     this.address={
      StreetNo:102,
      city:'Hyderabad',
      state:'Telangana',
     };
     this.hobbies=['sleeping' , 'writing','reading']
  
  }
 ngOnInit(){
   //alert("ngOnInit invoked...."); 
  }
}

